# UsingGit
## Using Git to Contribute to Open Source


### Why Yet Another Git Guide?

Learning _fundamental_ principles allows the student to:  

   1. Learn anything faster, 
   
   2. Extrapolate more easily into it's (their) facile, even creative, use, and 
   
   3. Few other guides provide these basics without excessive detail.  

---

### Other Good Git Guides  

   * [http://cs61.seas.harvard.edu/wiki/2012/Git](http://cs61.seas.harvard.edu/wiki/2012/Git "Harvard CS61 guide to git")  
   
   * [http://git.huit.harvard.edu/guide/](http://git.huit.harvard.edu/guide/ "git - the simple guide")  
   
   * [https://github.com/training-kit/](https://github.com/training-kit/ "Github Training Kit")
   
---

### The Four Confusions of Git

   1. [Why can't I check-out](./Answers/WhyCantICheckOut.md)
   
   2. [Where is the source code](./Answers/WhereIsTheSourceCode.md)?
   
   3. [Why isn't Commit enough](./Answers/WhyIsntCommitEnough.md)?
   
   4. [What is a Branch](./Answers/WhatIsABranch.md)?
   
---

### The Principles

   * You get your own copy of EVERYTHING(!) in a __**repository**__.
   
   * Making your own local copies is easy and are called __**branches**__.  
   
   * Saving changes on a __**branch**__ is called a __**commit**__.  

   * Your changes must be __**staged**__ before they are __**committed**__.  
   
   * __**Merges**__ are cheap.
   
---

### Saving a Thousand Words aboout Commit History

![freytag.us commit history](./Images/Screenshots/freytagDOTusCommitHistory.PNG)

---

### Copying a repository

   * 'git clone'

      * Make a complete copy of every __**commit**__ and __**branch**__ in a __**repository**__.

   * Steps

     1. surf to https://bitbucket.org/rchfreytag/UsingGit-20170404/overview

     2. select the HTTPS reference in the upper right.

     3. in your shell type 'git clone <https reference>'.

     	* https://rchfreytag@bitbucket.org/rchfreytag/usinggit-20170404.git  

	   * __Pro tip__:  If you wish to rename or relocate the resulting code repo in some place other than where you are presently use:

	   > git clone https://bitbucket.org/rchfreytag/UsingGit-20170404/overview  ~/git/novalabs/classes/  UsingGit

	   Broken down this is cloning the same directory as mentioned above yet explictly  declaring that it shall be cloned to '~/git/novalabs/classes/'  with the name __**UsingGit**__.
		
---

### Recovering Your Files from the Repository

   1. 'cd' into the source (working) directory.  

   2. 'ls' and note the files listed.  

   3. 'rm -rf ./*'  

   4. 'ls' and note the empty (working) directory.  

   5. 'git reset --hard HEAD'
   
      * _Use with extreme caution, __everything__ not commited and done __after__ the code at __**HEAD**__ __will__ be lost.  

   6. 'ls' note all the files re-listed.

---

### The Working Directory VS the Repository

   * The files you work with are temporary until __**committed**__ to the __**repository**__.

   2. 'cd' into your working directory.

      * Working directory is most akin to seeing your directory marked at the tail end of the filepath  in
      Windows Explorer/Spotlight/Thunar etc depending on your daily/preferred Operating System.

   3. 'touch eraseme.txt'

   4. 'ls' your working directory and confirm presence of 'eraseme.txt'.

   5. 'rm -rf ./*'

   6. 'reset --hard HEAD'

   * Note that 'eraseme.txt' is gone, gone, gone.

---

### Commit Early and Often

   1. Put your experiments on an experimental, ad hoc, branch until you are happy.

   2. Look at the __**commit**__ __**log**__ for this __**branch**__.

   3. Create end edit your files.

   4. Check your working diredtory __**status**__.

   5. Inspect your file changes if in doubt.

   6. __**Stage**__ your work.

   7. __**Commit**__ your work.

   8. Confirm your __**commit**__ worked.

---

### Create a __**Branch**__ for Your Changes

   1. 'git checkout -b <your name here>-questions-page'  

      * Note that the '-b' means create the __**branch**__ as you __**check**__ it out.  

      * Normally 'git checkout' would mean that any changes you had waiting would be 
      __**commit**__-ted to the pre-existing checked-out __**branch**__.  Here we are just
      saving the work to a __new__ branch.  

   2. 'git branch -a'

      * Provides a listing of ALL __**branches**__ including __**[remote tracking branches](./RemoteTrackingBranches.md)**__.

      * Making sure you _are_ on the branch you intend to __**commit**__ new work to; your
      working branch. This is another good reason to ADD the
      > -b
      ... above as it ensures you are in that branch.

      * This really happens - but not to you - you are amazing.  But, maybe do this just for grins.

---

### Look at the __**Commit**__ __**Log**__ for this __**Branch**__

   1. 'git log HEAD'

      * Uses 'less' or 'more' to show a list of every __**commit**__ on this __**branch**__.

      * The SHA1 has on each __**commit**__ can be used in place of HEAD or any branch name in most any command.  Shown as ** commit 0dd7c112a7bc34eb93cc5ea0513830acf987d2fd ** 

         * **Pause and think about all the places that SHA1 hashes can be used.**

	 * Only need to give the first ~5-6 letters of the SHA1 hash,  Enough to be distinct.

   2. 'git log <branch name>'

      * Gives the __**log**__ for any __**branch**__ even if not currently __**checked**__ out.

---

### Create and Edit Your Files

   1. 'vi Questions.md'

      * Now create your page of questions and put some content in there.  I suggest putting three  
      spaces, then a '*' in the left margin, typing your question, ending with two spaces and repeating.

   2. Save by typing the following:  (using vi/vim,adapt or ask about your text editors' complementary commads if unsure.)  

      1. ESC

         * Exits edit (INSERT) mode
      
      2. ':wq'  * writes the file to disk, and tehn quits the text editor, for vi :x does these both in one step

      3. 'ls' list the contents of the directory to confirm sucessful save of Questions.md

   * You should see 'Questions.md' in the directory next to README.md

   * _Save some questions for the next step._

---

### Check your working directory __**status**__.

   * 'git status'

      * Type this early and often.

      * Use this to figure out if you have staged the right files.

![freytag.us git_log_signed](./Images/Screenshots/gitlogsigned.PNG)

---

### Un-__**stage**__ Everything Prior to __**Stage**__-ing the Files for this __**Commit**__

   1. 'git branch'

      * Double- and triple-check you are on the branch you want.

      * Note the name of the current branch.

      	* It is the one with the '*' in the left margin.

   2. 'git reset --soft <the name of your current branch>'

      * This will un-__**stage**__ every file.

      * Your working directory with its changes will be untouched.

      * Don't delete your working directory because it isn't committed yet.

      	* Who would do a silly thing like that. <yeaaaaah>

      * Don't type 'git reset --hard HEAD' or your __changes are completely lost!__

---

### Inspect Your File Changes if in Doubt

   * 'git diff HEAD'

     * Compares your working directory to the files saved (__**commit**__-ed) to the repository.  
       This is the __**HEAD**__ of your current working branch.  

     * Use this to decide which files to __**stage**__ and then __**commit**__.  

     * Lines starting with '-' mean they are _missing_ from your current branch but __exist__ in the target branch_.

     * Lines starting with '-' mean they _exist_ in your current branch but are __missing__ in the target branch_.

   * 'git diff <branch name>  

     * Compares your working directory to the last commit of on the given <branch name>.  

     * Note the similarity to 'git diff HEAD'.  

---

### __**Stage**__ Your Work

   1. 'git status'  

      * shows what files are:  

      	* __**untracked**__.  
	* __**tracked**__ but not __**staged**__.  
	* __**tracked**__ and __**staged**__.  

   2. 'git add <filename>'  OR 'git add .'  in the parent directory (IF you wish to commit all of them as one commit, this may not always be the case. 
 
      * <filename> is _not_ yet saved (__**committed**__) to the __**repository**__

      * <filename> _IS_ ready to be __**committed**__.

---

### Inspect Your __**Stage**__-d Changes

   1. 'git diff --cached HEAD'

      * '--cached' means __**stage**__-d.

      * Shows the __**stage**__-d changes, line-by-line, ready for __**commit**__.

---

### __**Commit**__ Your Work  * Pro Tip from earlier applies here even for committing branches/tags.

   1. 'git commit -m "<add a decriptive one-line comment to help the next developer>"'

      * Don't put a '.' at the end of your comment.

      * Do make your comment descriptive and unique. ** If you need or desire more than one line, use a text editor ( leave out the <-m > and your defualt one should open for this purpose, OR if you prefer the command line   use the common < \ > to denote a 'carriage return' and continue on the next line'

   2. 'git log'

      * Make sure the commit took.

   3. 'git status'

      * Make sure your working directory and staged files are as you expect them.
       ** Pro Tip: Commiting Some files ( especially with large changes, singularly will leave some in staging area, this means more commits **BUT** makes a revert (if needed) easier.
---

## __**You have made a contribution!**__

---

### Walkthrough of the .git Directory

   * http://gitready.com/advanced/2009/03/23/whats-inside-your-git-directory.html

---

### Basic Configurations



   * Your name

     * git config --local user.name "John Doe"
	
     * git config --global org.name "ABC Corp"

   * Email

     * git config --local user.email johndoe@example.com
	
     * git config --global support@abccorp.com

   * Preferred Editor

     * git config --global core.editor emacs

   * Project URL
   
   * git config --global org.url https://developer.abccorp.com

   * [git configuration details!](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration)

   * => *now look in .git/config and note the changes!*  This is the other way to view your existing settings made above.


## Some Pro Tips

   1. Multiple values for everything shown below can be given in the case of teams. Organizations can have these as well as the individual contributors. 

   2. __**--global**__ is for all users/contributors of this repo, using __**--local**__ makes these changes _only_ in your local copies and _is not_ sent to any other branches during commits.

   3. Check your existing settings with git config --global (or --local) -l 

---

### Add Another __**Remote Tracking Branch**__

   1. 'git remote add <local name you want to assign the remote repository> https://rchfreytag@bitbucket.org/rchfreytag/usinggit.git'

      * This adds a __**remote**__ tracking reference in .git/config.
     
         * Tracking a **remote** branch allows you to checkout/clone/merge, and make pull requests with your code on the remote, which _does not_ have to be within the project.  A common example of that would be adding some feature or bug fix from another project.

      * _Go ahead and open '.git/config' and see what has changed.

   2. 'git branch -a'

      * __Nothing has changed!__  Why are there no branches?

      * Because you haven't pulled ('fetch'-ed) data from the new __**remote**__

   3. 'git fetch -a'

      * 'fetch' updates all __**remotes**__ with their respective __**remote tracking branches**__.

   4. 'git branch -a'

      * Note all the new __**branches**__ starting with the __**remote**__ name you assigned.

---

### Make a __**Local Branch**__ for the new __**Remote Tracking Branch**__

** Note: This allows you to make changes on your local copy of this remote branch __WITHOUT__ fear of inadvertently messeing with the remote branch.

   1. 'git checkout <name of new __**remote tracking branch**__'

      * Your current branch is now the new __**remote tracking branch**__.

      * You should not __**commit**__ to this branch.

      * _YOu have created a new local branch before.  Do that again._

   2. 'git checkout -b <new __**local**__ name you want to give to the new __**remote tracking branch**__>

      * NOTE: you will have to give this new __**branch**__ a unique name.
      For example:  git checkout -b shiny_feature 

      * __**Commit**__ to this __**branch**__ like any other.

      * You can 'git push' this __**local branch**__ to any other __**remote reference**__

---

### Pull and Push to the new __**Remote Tracking Branch**__

   1. 'git pull <name of new __**remote tracking branch**_>'

      * Pull first to make sure that no changes need to be applied.

   2. 'git push <name of new __**remote tracking branch**_>'

      * _If_ all is well you can just push up and there should be no problems.

## Pro Tip

   * Using ...  
   > __**git push  --set-upstream <remote_repo/remote_branch>**__
   will automate the process of pushing/pull/fetch/merge without the confusion of which branch it's going to, so check this thrice the first time and forget about it.  

	For example: `git push  --set-upstream  remote/origin/shiny_master`  will ensure all default actiions (fetch/push/pull/merge) will use that repo and branch
---

### __**Merging**__ with __**Conflicts**__


* Note: If multiple contributors are attempting to push to teh same repo and branch at the same time, this __CAN_ create a conflict known as a 'race condition' which as the name implies is when it is quite litereally a race to push, before someone else does, requiring another pull / push from you.

   1. 'git merge <name of branch you want to merge with>'

      * Resolve any differences between remote and local files (__**conflicts**__) by:

      	1. Editing each of the listed files.

	2. Picking each of the conflicting lines and selecting the line to keep (remote or local).

	3. Any other edits?  Make them now.

   2. 'git add <edited files>'

      * Now your __**merge commit**__ is __**staged**__

   3. 'git commit'

   4. 'git push <name of __**remote tracking branch**__>'

      * This should work with NO problems because you just carried out a merge.

---

### Now issue a __**Pull Request**__.
