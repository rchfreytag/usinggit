### Question: Where is the source code?

Generally the source code is what you will be cloning/checking out and otherwise working with. This is often either directly on teh Project's main page or linked to. for example:   

   * [https://github.com/pithos/pithos/](https://github.com/pithos/pithos/)
   "Pithos, open source Pandora Desktop Client for Linux") which is linked at their  

   * [https://pithos.github.io](https://pithos.github.io)
   Pithos "Pandora Dekstop Client for linux") webpage
    